//////////////////////////////////////////////////////
// Lily Mendoza
// CS 46J
// 9/19/19
// This program prompt the user to enter an integer
// and will take that integer and find all the prime
// numbers up to that integer.
//////////////////////////////////////////////////////

import java.util.Scanner;               // importing package to user Scanner

public class E6_5 {
    public static class PrimeGenerator {
        int userInt;
        String numbers = "";
        boolean prime;

        PrimeGenerator() {
            // prompting user to enter an integer
            System.out.println("Please enter an integer: ");
            // using scanner to take users integer
            Scanner in = new Scanner(System.in);
            userInt = in.nextInt();
            nextPrime(userInt);
        }

        void nextPrime(int userInt) {
            // checking to make sure users integer is greater than 2 or 3
            // adding prime numbers 2 and 3 manually
            if (userInt >= 2)
                numbers = numbers + "2 ";
            else if (userInt >= 3)
                numbers = numbers + "2 3 ";
            for (int i = 5; i <= userInt; i++) {
                isPrime(i);
                // adding i to numbers if prime is true
                if (prime)
                    numbers = numbers + Integer.toString(i) + " ";
            }
        }
        void isPrime(int i) {
            // initializing string called numbers to hold prime numbers
            prime = true;
            // looping through numbers up to users integer to find primes
            for(int j = 2; j <= i/2; j++) {
                prime = true;           // reseting prime to true
                if ((i % j) == 0) {
                    prime = false;      // checking if integer i is prime
                    break;
                }
            }
        }
        void print() {
            System.out.println(numbers);
        }
    }
    public static void main(String[] args) {
        PrimeGenerator newInt = new PrimeGenerator();
        // printing primes;
        newInt.print();
    }

}
